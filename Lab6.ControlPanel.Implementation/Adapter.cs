﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using Myjnia;
using System.Windows;

namespace Lab6.ControlPanel.Implementation
{
    public class Adapter:IControlPanel
    {
        IMyjnia myj;
        Window window;
        public Adapter(IMyjnia myjnia)
        {
            this.window = new MainWindow(myjnia);
            this.myj = myjnia;
        }
        public Window Window
        {
            get { return window; }
        }
        
    }
}
